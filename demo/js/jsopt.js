$(function() {
          $('a[rel*=modal]').leanModal({ closeButton: ".modal-close" });
          $('.close').click(function(){
            $(this).parent().hide();
          });
          $("#slider").owlCarousel({
            //items: 3,
            //itemsDesktop: false,
            //itemsTablet: [720,1],
            //itemsMobile: false,
            //slideSpeed: 200,
            autoPlay: true
          });
          var headerHeight = 0;
          $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
              if (target.length) {
                $('html,body').animate({
                  scrollTop: target.offset().top - headerHeight
                }, 1000);
                return false;
              }
            }
          });
      });