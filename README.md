# LANDING PAGE STYLE AND TEMPLATES #

[Documentation](http://digitalcreative.gannett.com/landing/tutorial/)

### What is this all about? ###

* This repository contains a general stylesheet, as well as template specific styles.
* It also contains SASS files to easily edit general settings.

**Each time you start a new landing page, you should download this repo to a new directory!**

### How do I get set up? ###

* Have SASS installed. [Start here](http://sass-lang.com/install) at "2. Command Line"
* With Terminal window still open, go to your directory.
* Type sass --watch scss:css

### Great, I need to make sense of these files! ###

* Training will show you which files are important. custom.scss is a file that generates a css -- not tied to a specific template.
* [Bootstrap](http://getbootstrap.com) takes care of all of the general styles, and as it the files are linked from their CDN servers, there are no additional css or js files to upload past the custom.css. There are backup CSS and JS files just in case, though.
